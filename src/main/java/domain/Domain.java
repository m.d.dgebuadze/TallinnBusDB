package domain;

import controller.BusController;
import controller.DriverController;
import controller.PassengersController;
import controller.TimetableController;

public class Domain {

	public static void main(String[] args) {


		DriverController driverController = new DriverController();
		//BusController busController = new BusController();
		TimetableController timetableController = new TimetableController();

		//driverController.listAllDrivers();
		//busController.listAllBuses();

		//System.out.println("Let's add a new Driver");
		//driverController.createDriver("Ivanka Trump", "Tall building", "+1002555",25,174);
		//driverController.listAllDrivers();
		//driverController.findDriverByBusNumber("20B");
		//driverController.findDriverByBusNumber("20A");

		System.out.println("Let's add a new Location");
		timetableController.createTimetable(1, 1, "Monday", "07:25:00");
		timetableController.listAllTimetables();

		timetableController.findTimetableForBusNumber("20a");
		timetableController.findTimetableForBusNumber("20b");

		timetableController.deleteTimetable(3);
		timetableController.insertToTimetable(2, 4, "Monday", "22:22:22");
		timetableController.updateTimetable(4, 7, "Sunday", "10:19:10");

	}
}
